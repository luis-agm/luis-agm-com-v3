# My Personal Website

[![pipeline status](https://gitlab.com/luis-agm/luis-agm-com-v3/badges/master/pipeline.svg)](https://gitlab.com/luis-agm/luis-agm-com-v3/-/commits/master)

Personal site, published at [luis-agm.dev](https://luis-agm.dev). Built initially in VueJS 1, migrated to NuxtJS with Vue 2 and progressively migrating to Composition API in preparation for Nuxt 3.

Now includes a small game made in Rust and compiled to WASM!

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).